package com.prueba.liverpool.Data;

import java.io.Serializable;
import java.io.SerializablePermission;

/**
 * Created by mariofernandezbermudez on 23/12/17.
 */

public class Product implements Serializable {

    private String productName;
    private String productPrice;
    private String productUbication;
    private String productImage;

    public Product(){
        productName = "";
        productPrice = "";
        productUbication = "";
        productImage = "";
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUbication() {
        return productUbication;
    }

    public void setProductUbication(String productUbication) {
        this.productUbication = productUbication;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
}

package com.prueba.liverpool.Utils;

import android.util.Log;

import com.prueba.liverpool.Data.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mariofernandezbermudez on 23/12/17.
 */

public class Utils {

    public static String formatStringParameters(String valuesString) {
        String formated = "";
        try {
            if (valuesString != null && valuesString != "")
                formated = valuesString.replace("[", "").replace("]", "").replace("\\", "");
            if (formated.length() > 2)
                formated = formated.substring(1, (formated.length() - 1));
        } catch (Exception e) {
            Log.e(Constant.TAG, Constant.TAG + " ======>>> " + e.getMessage());
        }
        return formated;
    }

    public static List<Product> formatJsonToProductList(String jsonRequest) {
        List<Product> products = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonRequest);
            JSONArray arrayContents1 = jsonObject.getJSONArray("contents");
            JSONObject maintContent1 = arrayContents1.getJSONObject(0);
            JSONArray arrayContents2 = maintContent1.getJSONArray("mainContent");
            JSONObject maintContent2 = arrayContents2.getJSONObject(1);
            JSONArray arrayContents3 = maintContent2.getJSONArray("contents");
            JSONObject maintContent3 = arrayContents3.getJSONObject(0);
            JSONArray arrayContents4 = maintContent3.getJSONArray("records");
            if (arrayContents4.length() > 0) {
                for (int i = 0; i < arrayContents4.length(); i++) {
                    Product producto = new Product();
                    JSONObject record = arrayContents4.getJSONObject(i);
                    JSONObject attributes = record.getJSONObject("attributes");
                    Utils.formatStringParameters(attributes.getString("product.displayName"));
                    producto.setProductName(Utils.formatStringParameters(attributes.getString("product.displayName")));
                    producto.setProductPrice(Utils.formatStringParameters(attributes.getString("sku.sale_Price")));
                    producto.setProductImage(Utils.formatStringParameters(attributes.getString("sku.thumbnailImage")));
                    products.add(producto);
                    //Log.i(Constant.TAG, "========================= ");
                }
            }
        } catch (Exception e) {
            Log.i(Constant.TAG, "========================= Error: "+e.getMessage());
        }


        return products;
    }
}

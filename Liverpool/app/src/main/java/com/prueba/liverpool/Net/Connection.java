package com.prueba.liverpool.Net;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.prueba.liverpool.activities.MainActivityList;

import org.json.JSONObject;

/**
 * Created by mariofernandezbermudez on 23/12/17.
 */

public class Connection {
    //final String url = "https://www.liverpool.com.mx/tienda/?s=&d3106047a194921c01969dfdec083925=json";
    final String url="https://www.liverpool.com.mx/tienda/?s=";
    final String url_Complete="&d3106047a194921c01969dfdec083925=json";
    public Context context;
    RequestQueue queue ;

    public Connection(Context contexto){
        context =contexto;
        queue = Volley.newRequestQueue(context);
    }

    public void getRequest(String search){
        String url_request ="";
        url_request = url.concat(search).concat(url_Complete);
        Log.i("Response", "===========>>>>> url_request  "+url_request);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url_request, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.i("Response", "===========>>>>> response from getRequest:  "+response.toString());
                        String responseJson =response.toString();
                        ((MainActivityList)context).processJson(responseJson);



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error.Response", "===========>>>>> response: "+error.toString());
                    }
                }
        );
        queue.add(getRequest);
    }

}

package com.prueba.liverpool.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.prueba.liverpool.R;

/**
 * Created by mariofernandezbermudez on 23/12/17.
 */

public class SplashActivity extends Activity{
    int secondsDelayed = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashActivity.this,
                        MainActivityList.class));
                finish();
            }
        }, secondsDelayed * 1000);

    }
}

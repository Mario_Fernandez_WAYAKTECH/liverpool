package com.prueba.liverpool.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prueba.liverpool.Data.Product;
import com.prueba.liverpool.Net.Connection;
import com.prueba.liverpool.adapter.ProductsAdapter;
import com.prueba.liverpool.R;
import com.prueba.liverpool.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mariofernandezbermudez on 23/12/17.
 */

public class MainActivityList extends AppCompatActivity {
    private List<Product> productList = new ArrayList<>();
    private ProductsAdapter mAdapter;
    private RecyclerView recyclerView;
    private EditText editTextSearch;
    private ImageView imageViewSearch;
    private Connection connection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        editTextSearch = (EditText) findViewById(R.id.edt_searchtext);
        imageViewSearch = (ImageView) findViewById(R.id.img_search);
        mAdapter = new ProductsAdapter(productList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        connection = new Connection(MainActivityList.this);
        imageViewSearch.setOnClickListener(listener);
        editTextSearch.setOnEditorActionListener(listenerEdit);
        editTextSearch.clearFocus();
        getProducts();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    public View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            initRequest();
        }
    };

    public TextView.OnEditorActionListener listenerEdit = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
            if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                initRequest();
            }
            return false;
        }
    };

    private void getProducts() {
        connection.getRequest("Computadora");
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editTextSearch.getWindowToken(), 0);
    }

    public void initRequest() {
        if(!editTextSearch.getText().toString().isEmpty())
            connection.getRequest(editTextSearch.getText().toString());
        else
            Toast.makeText(this, R.string.mainactivity_error_tv_busqueda, Toast.LENGTH_LONG).show();
    }

    public void processJson(String json) {
        List<Product> productListFormat = new ArrayList<>();
        productListFormat = Utils.formatJsonToProductList(json);

        if(productListFormat.size()>0){
            productList.clear();
            productList.addAll(productListFormat);
            this.mAdapter.notifyDataSetChanged();
        }
        else{
            Toast.makeText(this, R.string.mainactivity_error_busqueda, Toast.LENGTH_LONG).show();
        }

    }


}

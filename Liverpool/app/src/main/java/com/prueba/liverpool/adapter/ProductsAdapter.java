package com.prueba.liverpool.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.prueba.liverpool.Data.Product;
import com.prueba.liverpool.R;

import java.util.List;

/**
 * Created by mariofernandezbermudez on 23/12/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {
    private List<Product> productList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView productName, productPrice, productUbication;
        public ImageView productImage;

        public MyViewHolder(View view) {
            super(view);
            productName = (TextView) view.findViewById(R.id.tv_productname);
            productPrice = (TextView) view.findViewById(R.id.tv_productprice);
            productUbication = (TextView) view.findViewById(R.id.tv_productubication);
            productImage = (ImageView)view.findViewById(R.id.img_productimage);
        }
    }


    public ProductsAdapter(List<Product> productList,Context context) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_products, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.productName.setText(product.getProductName());
        holder.productPrice.setText(product.getProductPrice());
        //holder.productUbication.setText(product.getProductUbication());
        Glide.with(context)
                .load(product.getProductImage())

                .apply(RequestOptions.centerCropTransform())
                .into(holder.productImage);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}
